let headRow = document.getElementsByClassName('head_row');
let add = document.getElementById('add');
let updateTable = document.getElementById('update');
let clear = document.getElementById('clear');
let bodyRow = document.getElementsByClassName('body_row');

let studentArray = localStorage.getItem('items') ? JSON.parse(localStorage.getItem('items')) : [];

localStorage.setItem('items', JSON.stringify(studentArray));

let data = JSON.parse(localStorage.getItem('items'));

let thMaker = date => {
    for(let x = 0; x < headRow.length; x++) {
        let th = document.createElement('th');
        let inputDate = document.createElement('input');
        inputDate.className = "dateAdd";
        inputDate.type = 'date'
        th.textContent = date;
        th.appendChild(inputDate);
        headRow[x].appendChild(th);
    }
    
}

let tdMaket = gradeInput => {
    for(let i = 0; i < bodyRow.length; i++) {
        let td = document.createElement('td');
        
        let input = document.createElement('input');
        input.className = "gradeAdd";
        input.type = "number"
        td.textContent = gradeInput;
        td.appendChild(input);
        bodyRow[i].appendChild(td);
    }
}

function insertion(nums, trs) {
	nums.forEach((num, index) => {
        let td = document.createElement('td');
        td.className = "sashvalo" 
        td.textContent = num;
		trs[index].appendChild(td);
	})
}

function insert(nums, trs) {
	nums.forEach((num, index) => {
        let th = document.createElement('th'); 
        th.className = "totalday";
        th.textContent = num;
		trs[index].appendChild(th);
	})
}

add.addEventListener('click', function(e) {
    e.preventDefault();
    thMaker();
    tdMaket();
});

let gradeAdd = document.getElementsByClassName('gradeAdd');
let dateAdd = document.getElementsByClassName('dateAdd');


updateTable.addEventListener('click', function(t) {
    t.preventDefault();

    let newArray = {};
    let tarigebi = []
    newArray.date = tarigebi
    for(let n = 0; n < dateAdd.length; n++) {
        tarigebi.push(dateAdd[n].value);
    }

    let qulebi = [];
    newArray.grade = qulebi;
    for(let k = 0; k < gradeAdd.length; k++) {
        qulebi.push(gradeAdd[k].value);
    }

    data.push(newArray);
    localStorage.setItem('items', JSON.stringify(data));
    location.reload();
});

data.forEach(item => {
    insert(item['date'],headRow);
    insertion(item['grade'],bodyRow);
    
});

let deleteDay = document.getElementById('delete');

deleteDay.addEventListener('click', () => {
    deletedata = JSON.parse(localStorage.getItem('items'));
    deletedata.pop()
    localStorage.setItem('items', JSON.stringify(deletedata));
    location.reload();
    
})

let totalDate = document.getElementById('totaldate');
let totalDay = document.getElementsByClassName('totalday');
totalDate.innerText = totalDay.length;

let totalStudent = document.getElementById('totalstd');
let totalStd = document.getElementsByClassName('stdid');
totalStudent.innerText = totalStd.length;

let bkgColor = document.getElementsByClassName('sashvalo');

let colorArray = []
for(let x = 0; x < bkgColor.length; x++) {
    colorArray.push(bkgColor[x]);
    if(bkgColor[x].outerText == 0) {
        bkgColor[x].classList.add('feri2');
    } else {
        bkgColor[x].classList.add('feri');
    }
}

let missed = document.getElementById('missed')

let missedArray = []
for(let y = 0; y < colorArray.length; y++) {
    missedArray.push(colorArray[y].outerText)
}

function isBigEnough(value) {
    return value == 0;
}
  
let filtered = missedArray.filter(isBigEnough);
missed.innerText = filtered.length;



let avgTr1 = document.getElementById("avgtr1");
let average1 = document.getElementById('avg1')
avgValue1 = avgTr1.getElementsByClassName("sashvalo")
let avgArray1 = []
for(let i = 0; i < avgValue1.length; i++) {
    avgArray1.push(avgValue1[i].outerText)
}
let avgArr1 = []
for(let i = 0; i < avgArray1.length; i++) {
    avgArr1.push(parseInt(avgArray1[i]))
}
let total1 = 0;
for(var i = 0; i < avgArr1.length; i++) {
    total1 += avgArr1[i];
}
let avg1 = total1 / avgArr1.length;
if(!avg1) {
    average1.innerText = total1.toFixed(2);
} else {
    average1.innerText = avg1.toFixed(2);
}



let avgTr2 = document.getElementById("avgtr2");
let average2 = document.getElementById('avg2')
avgValue2 = avgTr2.getElementsByClassName("sashvalo")
let avgArray2 = []
for(let i = 0; i < avgValue2.length; i++) {
    avgArray2.push(avgValue2[i].outerText)
}
let avgArr2 = []
for(let i = 0; i < avgArray2.length; i++) {
    avgArr2.push(parseInt(avgArray2[i]))
}
let total2 = 0;
for(var i = 0; i < avgArr2.length; i++) {
    total2 += avgArr2[i];
}
let avg2 = total2 / avgArr2.length;
if(!avg2) {
    average2.innerText = total2.toFixed(2);
} else {
    average2.innerText = avg2.toFixed(2);
}

let avgTr3 = document.getElementById("avgtr3");
let average3 = document.getElementById('avg3')
avgValue3 = avgTr3.getElementsByClassName("sashvalo")
let avgArray3 = []
for(let i = 0; i < avgValue3.length; i++) {
    avgArray3.push(avgValue3[i].outerText)
}
let avgArr3 = []
for(let i = 0; i < avgArray3.length; i++) {
    avgArr3.push(parseInt(avgArray3[i]))
}
let total3 = 0;
for(var i = 0; i < avgArr3.length; i++) {
    total3 += avgArr3[i];
}
let avg3 = total3 / avgArr3.length;
if(!avg3) {
    average3.innerText = total3.toFixed(2);
} else {
    average3.innerText = avg3.toFixed(2);
}


let avgTr4 = document.getElementById("avgtr4");
let average4 = document.getElementById('avg4')
avgValue4 = avgTr4.getElementsByClassName("sashvalo")
let avgArray4 = []
for(let i = 0; i < avgValue4.length; i++) {
    avgArray4.push(avgValue4[i].outerText)
}
let avgArr4 = []
for(let i = 0; i < avgArray4.length; i++) {
    avgArr4.push(parseInt(avgArray4[i]))
}
let total4 = 0;
for(var i = 0; i < avgArr4.length; i++) {
    total4 += avgArr4[i];
}
let avg4 = total4 / avgArr4.length;
if(!avg4) {
    average4.innerText = total4.toFixed(2);
} else {
    average4.innerText = avg4.toFixed(2);
}


let avgTr5 = document.getElementById("avgtr5");
let average5 = document.getElementById('avg5')
avgValue5 = avgTr5.getElementsByClassName("sashvalo")
let avgArray5 = []
for(let i = 0; i < avgValue5.length; i++) {
    avgArray5.push(avgValue5[i].outerText)
}
let avgArr5 = []
for(let i = 0; i < avgArray5.length; i++) {
    avgArr5.push(parseInt(avgArray5[i]))
}
let total5 = 0;
for(var i = 0; i < avgArr5.length; i++) {
    total5 += avgArr5[i];
}
let avg5 = total5 / avgArr5.length;
if(!avg5) {
    average5.innerText = total5.toFixed(2);
} else {
    average5.innerText = avg5.toFixed(2);
}

let avgTr6 = document.getElementById("avgtr6");
let average6 = document.getElementById('avg6')
avgValue6 = avgTr6.getElementsByClassName("sashvalo")
let avgArray6 = []
for(let i = 0; i < avgValue6.length; i++) {
    avgArray6.push(avgValue6[i].outerText)
}
let avgArr6 = []
for(let i = 0; i < avgArray6.length; i++) {
    avgArr6.push(parseInt(avgArray6[i]))
}
let total6 = 0;
for(var i = 0; i < avgArr6.length; i++) {
    total6 += avgArr6[i];
}
let avg6 = total6 / avgArr6.length;
if(!avg6) {
    average6.innerText = total6.toFixed(2);
} else {
    average6.innerText = avg6.toFixed(2);
}


let avgTr7 = document.getElementById("avgtr7");
let average7 = document.getElementById('avg7')
avgValue7 = avgTr7.getElementsByClassName("sashvalo")
let avgArray7 = []
for(let i = 0; i < avgValue7.length; i++) {
    avgArray7.push(avgValue7[i].outerText)
}
let avgArr7 = []
for(let i = 0; i < avgArray7.length; i++) {
    avgArr7.push(parseInt(avgArray7[i]))
}
let total7 = 0;
for(var i = 0; i < avgArr7.length; i++) {
    total7 += avgArr7[i];
}
let avg7 = total7 / avgArr7.length;
if(!avg7) {
    average7.innerText = total7.toFixed(2);
} else {
    average7.innerText = avg7.toFixed(2);
}


let avgTr8 = document.getElementById("avgtr8");
let average8 = document.getElementById('avg8')
avgValue8 = avgTr3.getElementsByClassName("sashvalo")
let avgArray8 = []
for(let i = 0; i < avgValue8.length; i++) {
    avgArray8.push(avgValue8[i].outerText)
}
let avgArr8 = []
for(let i = 0; i < avgArray8.length; i++) {
    avgArr8.push(parseInt(avgArray8[i]))
}
let total8 = 0;
for(var i = 0; i < avgArr8.length; i++) {
    total8 += avgArr8[i];
}
let avg8 = total8 / avgArr8.length;
if(!avg8) {
    average8.innerText = total8.toFixed(2);
} else {
    average8.innerText = avg8.toFixed(2);
}

let avgTr9 = document.getElementById("avgtr9");
let average9 = document.getElementById('avg9')
avgValue9 = avgTr9.getElementsByClassName("sashvalo")
let avgArray9 = []
for(let i = 0; i < avgValue9.length; i++) {
    avgArray9.push(avgValue9[i].outerText)
}
let avgArr9 = []
for(let i = 0; i < avgArray9.length; i++) {
    avgArr9.push(parseInt(avgArray9[i]))
}
let total9 = 0;
for(var i = 0; i < avgArr9.length; i++) {
    total9 += avgArr9[i];
}
let avg9 = total9 / avgArr9.length;
if(!avg9) {
    average9.innerText = total9.toFixed(2);
} else {
    average9.innerText = avg9.toFixed(2);
}


let avgTr10 = document.getElementById("avgtr10");
let average10 = document.getElementById('avg10')
avgValue10 = avgTr10.getElementsByClassName("sashvalo")
let avgArray10 = []
for(let i = 0; i < avgValue10.length; i++) {
    avgArray10.push(avgValue10[i].outerText)
}
let avgArr10 = []
for(let i = 0; i < avgArray10.length; i++) {
    avgArr10.push(parseInt(avgArray10[i]))
}
let total10 = 0;
for(var i = 0; i < avgArr10.length; i++) {
    total10 += avgArr10[i];
}
let avg10 = total10 / avgArr10.length;
if(!avg10) {
    average10.innerText = total10.toFixed(2);
} else {
    average10.innerText = avg10.toFixed(2);
}